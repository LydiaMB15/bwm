var map;
var infowindow;
var mijson;

function initMap() {
  // Creamos un mapa con las coordenadas actuales
  navigator.geolocation.getCurrentPosition(function (pos)  {

    lat = pos.coords.latitude;
    lon = pos.coords.longitude;

    //var myLatlng = new google.maps.LatLng(38.5075400, -0.2334600);
    var myLatlng = new google.maps.LatLng(document.getElementById('latitud').value,
                                          document.getElementById('longitud').value);

    var mapOptions = {
      center: myLatlng,
      zoom: 14,
      mapTypeId: google.maps.MapTypeId.SATELLITE
    };

    map = new google.maps.Map(document.getElementById('map'), {
      
      zoom: 6,
      center: 
            { 
              lat: parseFloat(document.getElementById('latitud').value), 
              lng: parseFloat(document.getElementById('longitud').value) 
            },
      mapTypeControl: true,
      mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
        mapTypeIds: ['roadmap', 'terrain']
      }
    });


    // Creamos el infowindow
    infowindow = new google.maps.InfoWindow();
   

    // Especificamos la localización, el radio y el tipo de lugares que queremos obtener
    var request = {
      location: myLatlng,
      radius: 50000,
      types: ['shopping_mall'],
      name: ['Centro Comercial']

    };

    // Creamos el servicio PlaceService y enviamos la petición.
    var service = new google.maps.places.PlacesService(map);

    service.nearbySearch(request, function (results, status) {
      if (status === google.maps.places.PlacesServiceStatus.OK) {
        for (var i = 0; i < results.length; i++) {
          crearMarcador(results[i], i);
        }
      }else{
        alert('Google maps no encuenta centros comerciales cerca de tu ubicación');
      }
    });
  });



}

//############### Save Marker Function ##############
function save_marker(marker, place, i) {
  //Save new marker using jQuery Ajax
  
  var mLatLng = marker.getPosition().toUrlValue(); //get marker position
  var myData = { id: i, 
                name: place.name, 
                address: place.vicinity, 
                latlong: mLatLng, 
                idG: place.id, 
                types: place.types[0] }; //post variables

  $( document ).ready(function() {     
    $.ajaxSetup
    ({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax
    ({        
      url: '/mapa/recogerForm',
          method: 'get',
          data: myData,
      
          success: function(ok)
          {              
            console.log(ok);
            //Generar div con datos             
            $('.list-group').append('<a href="/procesar/{'+
                                    myData["idG"]+'}" id="'+
                                    myData["idG"]+'" class="list-group-item list-group-item-action">'+
                                    myData["name"] + " , " + 
                                    myData["address"]+'</a>');  
          },
          error: function (xhr, ajaxOptions, thrownError) {
            alert(thrownError); //throw any errors
      }});
    });
}

function crearMarcador(place, i) {
  // Creamos un marcador
  var marker = new google.maps.Marker({
    map: map,
    position: place.geometry.location

  });

  // Asignamos el evento click del marcador
  google.maps.event.addListener(marker, 'click', function () {
    infowindow.setContent(place.name);
    infowindow.open(map, this);

  });
  
  save_marker(marker, place, i);

}





