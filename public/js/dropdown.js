jQuery(function($){
    $.ajax({
        type: 'GET',
        url: '/comunidades/',

        success: function(response) 
        {

            $("#cbx_comunidad").empty();
            console.log(response);
            $("#cbx_comunidad").prepend("<option value='' selected='selected'>Seleccione Comunidad</option>");
            for (i = 0; i < response.length; i++) 
            {
                $("#cbx_comunidad").append("<option value='" + "rellena una opcion" + "</option>"); 
                //console.log('response' + response[i].id);
                $("#cbx_comunidad").append("<option value='" + 
                                            response[i].id + "'>" + 
                                            response[i].comunidad +
                                            "</option>");                 
            } 
                
            let value = $("#cbx_comunidad").attr('data-id');
            $('#cbx_comunidad option').each(function () {
                if (($(this).attr('value')) === value) 
                {

                    $(this).attr('selected', 'selected');
                    $('#cbx_comunidad').trigger("change");

                }    
            }); 
            
        }
    });
});
    

$('#cbx_comunidad').on('change', function(event) {
    $.ajax({
    type: 'GET',
    url: '/provincias/' + event.target.value,

    success: function(response) {
        $("#cbx_provincia").empty();
        // console.log(response);
        $("#cbx_provincia").prepend("<option value='' selected='selected'>Seleccione Provincia</option>");
        for (i = 0; i < response.length; i++) {
            $("#cbx_provincia").append("<option value='" + "rellena una opcion" + "</option>");
            $("#cbx_provincia").append("<option value='" + 
                                        response[i].id + "'>" + 
                                        response[i].provincia + 
                                        "</option>");
                
        }
        
        let value = $("#cbx_provincia").attr('data-id');
        $('#cbx_provincia option').each(function () 
        {
            if (($(this).attr('value')) === value) 
            {
                $(this).attr('selected', 'selected');
                $('#cbx_provincia').trigger("change");
            }    
        });
        
    }   
    });
});


$("#cbx_provincia").change(function (event) {
    //alert( 'hola' + event.target.value ); 
    $.get("/municipios/" + event.target.value + "", function (response, provincia) {
        $("#cbx_municipio").empty();
        $("#cbx_municipio").prepend("<option value='' selected='selected'>Seleccione Municipio</option>");
        for (i = 0; i < response.length; i++) {
            $("#cbx_municipio").append("<option value='" + 
                                        response[i].id + "{" + 
                                        response[i].latitud + "{" + 
                                        response[i].longitud + "'>" + 
                                        response[i].municipio + 
                                        "</option>");           
        }

        let value = $("#cbx_municipio").attr('data-id');
            $('#cbx_municipio option').each(function () 
            {
                var cadena = $(this).attr('value');
                var pos = cadena.indexOf("{");
                if (($(this).attr('value').substring(cadena, pos)) === value) 
                {
                    $(this).attr('selected', 'selected');
                   // $('#cbx_municipio').trigger("change");
                
                }    
        });
    });
});


$("#cbx_municipio").change(function (event) {


    
    var selectedMunicipio = $(this).children("option:selected").val();
    //alert("You have selected the country - " + selectedMunicipio);
    var pos = selectedMunicipio.indexOf("{");
    var pos2 = selectedMunicipio.lastIndexOf("{");
    //alert('pos ' + pos +1 + 'pos2 ' + pos2 +1);
    var lat = selectedMunicipio.substring(pos + 1, pos2 - 1);
    var long = selectedMunicipio.substring(pos2 + 1, selectedMunicipio.length);
    //alert('latitud ' + lat + 'longitud ' + long);

    $(longitud).val("");
    $(latitud).val("");
    $(longitud).val(long);
    $(latitud).val(lat);
  
            

});


