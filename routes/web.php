<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'ComunidadController@index')->name('index');

Route::get('/comunidades','ComunidadController@getComunidades');

Route::get('/provincias/{id}','ProvinciaController@getProvincias');
Route::get('register/provincias/{id}','ProvinciaController@getProvincias');

Route::get('/municipios/{id}','MunicipioController@getMunicipios');
Route::get('register/municipios/{id}','MunicipioController@getMunicipios');

Route::get('/mapa/recogerForm','MapaController@recogerForm');

Route::post('/mapa/recogerForm','MapaController@recogerForm');
Route::get('/mapa', 'MapaController@viewLocations');

Route::get('/procesar/{id}','MapaProcesarController@procesarPeticion');



Route::get('/home', 'HomeController@index')->name('home');

Route::get('/blog', 'PagesController@home')->name('blog.show');

Route::get('blog/{post}','PostsController@show')->name('posts.show');
Route::get('categorias/{category}','CategoriesController@show')->name('categories.show');
Route::get('tags/{tag}','TagsController@show')->name('tags.show');

Route::group(['prefix' => 'admin', 'namespace' => 'Admin','middleware' => 'auth'],function(){

    Route::resource('posts', 'PostController',['except' => 'show', 'as' => 'admin']);
    Route::resource('users', 'UsersController',['as' => 'admin']);

    Route::put('users/{user}/roles', 'UsersRolesController@update')->name('admin.users.roles.update');
    Route::put('users/{user}/permissions', 'UsersPermissionsController@update')->name('admin.users.permissions.update');

    // Route::get('posts','PostController@index')->name('admin.posts.index');
    // Route::get('posts/create','PostController@create')->name('admin.posts.create');
    // Route::post('posts','PostController@store')->name('admin.posts.store');
    // Route::get('posts/{post}','PostController@edit')->name('admin.posts.edit');
    // Route::put('posts/{post}','PostController@update')->name('admin.posts.update');
    // Route::delete('posts/{post}', 'PostController@destroy')->name('admin.posts.destroy');


    Route::get('/','AdminController@index')->name('dashboard');

    Route::get('escrito','EscritController@index')->name('admin.escrito.index');
    Route::get('escrito/create','EscritController@create')->name('admin.escrito.create');

    Route::post('posts/{post}/photos','PhotosController@store')->name('admin.post.photos.update');
    Route::delete('photos/{photo}','PhotosController@destroy')->name('admin.photos.destroy');
});




Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');
