<div class="gallery-image masonry">
    @foreach($post->photos->take(4) as $photo)
    <figure class="img-responsive">
        <!-- @if($loop->index === 3)
					<div class="overlay">{{ $post->photos->count() }} Fotos </div>
				@endif -->
        <img src="{{ url($photo->url) }}" alt="">
    </figure>
    @endforeach
</div>