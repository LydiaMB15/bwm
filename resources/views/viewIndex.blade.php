<html>
</html>
<head>
    @yield('style')
</head>
<body>
</body>
<div class="container-fluid"> 
  
    @yield('nav')
    @yield('header')
    @yield('content')
    @yield('buy')  
    @yield('subfooter')
    @yield('footer')
    @yield('scripts')
    
</div>