<html>
@extends('viewIndex')

<head>
    @section('style')
        <meta name="viewport" 
              content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" 
              href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" 
              crossorigin="anonymous">
        <link rel="stylesheet" 
              href="https://fonts.googleapis.com/css?family=McLaren">
        <link href="https://fonts.googleapis.com/css?family=Quicksand&display=swap" 
              rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Big+Shoulders+Text&display=swap" 
              rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Liu+Jian+Mao+Cao&display=swap" 
              rel="stylesheet">
        <link rel="stylesheet" 
              href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" 
              href="/css/buywithme.css">
    @endsection


</head>

<body>

    <div class="container-fluid">
        @include('admin.partial.navBuyWithMe')
        @section('header')
        <div class="row rowCustomHeader">
            <div class="col12 col-sm-12 col-md-12 col-lg-2 d-none d-lg-block columna1 ">
                <img class="d-block_custom" 
                     src="/imagenes/chicaropa2.jpg" alt="">
                <ul class="text-center list-group list-group-flush">
                    <li class="list-group-item">
                        <a href="https://www.stradivarius.com/">
                            Stradivarius
                        </a>
                    </li>
                    <li class="list-group-item">
                        <a href="https://www.zara.com/us/es/">
                            Zara
                        </a>
                    </li>
                    <li class="list-group-item">
                        <a href="https://www.desigual.com/">
                            Desigual
                        </a>
                    </li>
                    <li class="list-group-item">
                        <a href="https://www.pullandbear.com/ic/">
                            Pullandbear
                        </a>
                    </li>
                    <li class="list-group-item">
                        <a href="https://www2.hm.com/">
                            H&M
                        </a>
                    </li>
                </ul>
            </div>

            <div class="col12 col-sm-12 col-md-12 col-lg-8 col-centered columna2">
                <div id="carouselExampleControls" 
                     class="carousel slide"
                     data-ride="carousel">
                     <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block_custom" 
                                 src="/imagenes/people-big2557483_1280.jpg" 
                                 alt="First slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block_custom" 
                                 src="/imagenes/bangkok-big3481970_1920.jpg" 
                                 alt="Second slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block_custom" 
                                 src="/imagenes/compras2.jpg" 
                                 alt="Third slide">
                        </div>
                        <a class="carousel-control-prev" 
                           href="#carouselExampleControls" 
                           role="button"
                           data-slide="prev">
                           <span class="carousel-control-prev-icon" aria-hidden="true">                               
                           </span>
                           <span class="sr-only">
                                Previous
                            </span>
                        </a>
                        <a class="carousel-control-next" 
                           href="#carouselExampleControls"
                           role="button"
                           data-slide="next">
                           <span class="carousel-control-next-icon" 
                                 aria-hidden="true">
                            </span>
                           <span class="sr-only">
                                  Next
                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col12 col-sm-12 col-md-12 col-lg-2 d-none d-lg-block columna3">
                <img class="d-block_custom" 
                     src="imagenes/chicoropa.jpg" 
                     alt="">
                <ul class="text-center list-group list-group-flush">
                    <li class="list-group-item">
                        <a href="https://shop.mango.com/us-es/hombre">
                            Mango Man
                        </a>
                    </li>
                    <li class="list-group-item">
                        <a href="https://myspringfield.com/es/es">
                            SpringField
                        </a>
                    </li>
                    <li class="list-group-item">
                        <a href="https://www2.hm.com/en_us/men/concepts/hm-men.html">
                            H&M Men
                        </a>
                    </li>
                    <li class="list-group-item">
                        <a href="https://www.zara.com/es/es/hombre-l534.html">
                            Zara Men
                        </a>
                    </li>
                    <li class="list-group-item">
                        <a href="https://www.jackjones.com/es/es/home">
                            Jack & Jones
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        @endsection
        @section('content')
        <div class="row rowCustom">
            @foreach($escritos as $escrito)
            @if($escrito->parte->name == 'triptico superior')
            <div class="col-12 col-sm-12 col-md-12 col-lg-4">

                <h3 class="text-center font-weight-bold">{{ $escrito->title}}</h3>
                <p class="parrafo">{!! $escrito->body !!}</p>
                <div class="text-center">
                    <a href="#" class="btn btn-info">{{ $escrito->button_title}}</a>
                </div>
            </div>
            @endif
            @endforeach

            <!-- <div class="col-12 col-sm-12 col-md-12 col-lg-4">
                <h3 class="text-center font-weight-bold">Soy Asesor</h3>
                    <p class="parrafo">Me divierto comprando, me divierto dándo a la gente lo mejor de mí, los consejos de moda que más me gustan, me gusta ser amable y que queden satisfechos con la compra que hacen. Me encanta cuando la gente está feliz gracias a mi ayuda y colaboración. A menudo me tomo un café con mis clientes y entablo amistades! Es la mejor forma de invertir mi tiempo!</p>
                    <div class="text-center">
                        <button class="btn btn-info">Quiero ser Asesor</button>
                    </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-4">
                    <h3 class="text-center font-weight-bold">Soy Comprador</h3>
                    <p class="parrafo">Me gusta tener a alguien que me guíe a la hora de comprar, pero no siempre mis conocidos pueden venir conmigo, me divierto cuando mi asesor me acompaña, saco provecho de las horas de compra, siempre me llevo algo que me encanta! me pruebo los modelos que mejor me quedan sin salir del probador! así da gusto comprar! </p>
                    <div class="text-center">
                        <button class="btn btn-info">Quiero ser Comprador</button>
                    </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-4">
                    <h3 class="text-center font-weight-bold">Soy Colaborador</h3>
                    <p class="parrafo">Mi comercio está disponible en esta web, porque me permite ofrecer a mis clientes un servicio de valor añadido muy apreciado por los consumidores. Ellos acuden a mi comercio, acompañados por los mejores asesores, que suerte que desde entonces mi comercio se llena! Esta web me ofrece la visibilidad que antes no tenía!</p>
                    <div class="text-center">
                        <button class="btn btn-info ">Quiero ser Colaborador</button>
                    </div>
            </div> -->
        </div>
        <div class="row rowCustom">
            <div class="col-12 m2 col-sm-12 col-md-4 d-flex justify-content-center">
                <div class="card" style="width: 18rem;">
                    <img src="/imagenes/stock-photo-man-and-woman-working-behind-the-counter-in-a-clothing-store-787054072.jpg"
                        class="card-img-top" alt="...">
                    <div class="card-body">
                        <p class="card-text">Si te gusta dar consejos sobre moda y estilos, tú también puedes ser
                            Asesor. Los asesores de nuestra web se llevan comisión por hacer lo que más les gusta! Echa
                            un vistazo a las asesorias actuales.</p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-4 d-flex justify-content-center">
                <div class="card" style="width: 18rem;">
                    <img src="/imagenes/stock-photo-amazed-shopper-opening-mouth-holding-shopping-bags-watching-special-offers-in-stores-and-pointing-520267153.jpg"
                        class="card-img-top" alt="...">
                    <div class="card-body">
                        <p class="card-text">Si te gusta comprar y te gusta ir acompañado,si te gusta oir los últimos
                            consejos sobre moda o necesitas a alguien que te asesore para un evento importante, alguien
                            que te lleve las bolsas de las compras y que te lleve las tallas al probador, regístrate
                            cómo Comprador! a qué esperas!</p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-4 d-flex justify-content-center">
                <div class="card" style="width: 18rem;">
                    <img src="/imagenes/business-3152586_1280.webp" class="card-img-top" alt="...">
                    <div class="card-body">
                        <p class="card-text">Sí quieres que tú comercio esté disponible en las búsquedas de nuestros
                            usuarios, estés o no en un centro comercial, seas un gran comercio como un pequeño comercio,
                            regístrate como Colaborador, y tus ventas se dispararán!</p>
                    </div>
                </div>
            </div>
        </div>
        @endsection
        @section('buy')
        <div class="row mibackground">

            <div class="col-12 col-sm-12 col-md-12 text-center">
                <h3 class="pregunta">¿Dónde quieres comprar?</h3>
            </div>

            <div class="col-12 col-sm-12 col-md-4">
                <div class="form-group">
                    <form action="{{ url('/mapa') }}" method="get">
                        {{ csrf_field() }}
                        <label for="exampleFormControlSelect1">Seleccione Comunidad:</label>
                        <select class="form-control @error('cbx_comunidad') is-invalid @enderror" name="cbx_comunidad"
                            id="cbx_comunidad" required>
  
                        </select>

                </div>
                @error('cbx_comunidad')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="col-12 col-sm-12 col-md-4">
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Seleccione Provincia:</label>
                    <select class="form-control @error('cbx_provincia') is-invalid @enderror" name="cbx_provincia"
                        id="cbx_provincia" required>

                    </select>
                </div>
                @error('cbx_provincia')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="col-12 col-sm-12 col-md-4">
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Seleccione Municipio:</label>
                    <select class="form-control @error('cbx_municipio') is-invalid @enderror" name="cbx_municipio"
                        id="cbx_municipio" required>
                        <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />

                        <input id="longitud" name="longitud" type="hidden" value="">
                        <input id="latitud" name="latitud" type="hidden" value="">

                    </select>
                </div>
                @error('cbx_municipio')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="col-12 col-sm-12 col-md-12 text-center">
                <input type="submit" class="btn btn-info mb-2" value="Buscar">
            </div>
        </div>
        @endsection
        @section('subfooter')
        <div class="row mibackground2">
            <div class="col-12 col-sm-12 col-md-12 text-center">
                <h3 class="pregunta2">Ventajas de utilizar BuyWithMe</h3>
            </div>
            <div class="col-12 col-sm-5 col-md-4 col-lg-3">
                <img class="imgIcon rounded mx-auto d-block"
                    src="/imagenes/1077966-ecommerce/1077966-ecommerce/png/017-shopping-bag.png">
                <p>Te asesorarán para que nunca falles en tus compras, un asesor te guía durante todo el proceso.</p>
            </div>
            <div class="col-12 col-sm-5 col-md-4 col-lg-3">
                <img class="imgIcon rounded mx-auto d-block"
                    src="/imagenes/1077966-ecommerce/1077966-ecommerce/png/044-stopwatch.png">
                <p>Ahorrarate tiempo en tus compras, el asesor te llevará la ropa al probador y las bolsas. </p>
            </div>
            <div class="col-12 col-sm-5 col-md-4 col-lg-3">
                <img class="imgIcon rounded mx-auto d-block"
                    src="/imagenes/1077966-ecommerce/1077966-ecommerce/png/045-speech-bubble.png">
                <p>Permítete el lujo de entablar conversación con tu asesor, es una oportunidad para hacer una bonita
                    amistad.</p>

            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                <img class="imgIcon rounded mx-auto d-block"
                    src="/imagenes/1179521-e-commerce/1179521-e-commerce/png/024-discount-2.png">
                <p>Llévate descuentos gratis de tus firmas favoritas, por ir de compras a través de BuyWithMe. </p>

            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                <img class="imgIcon rounded mx-auto d-block"
                    src="/imagenes/1179521-e-commerce/1179521-e-commerce/png/001-smartphone.png">
                <p>BuyWithMe es compatible con los dispositivos mobiles.</p>

            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                <img class="imgIcon rounded mx-auto d-block"
                    src="/imagenes/1077966-ecommerce/1077966-ecommerce/png/012-online-shopping.png">
                <p>También es compatible con los pc's. </p>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                <img class="imgIcon rounded mx-auto d-block"
                    src="/imagenes/1077966-ecommerce/1077966-ecommerce/png/007-piggy-bank.png">
                <p>Ahorra dinero en tus compras, el asesor dispondrá de un presupuesto máximo para que puedas compras
                    sin sustos</p>

            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                <img class="imgIcon rounded mx-auto d-block"
                    src="/imagenes/1077966-ecommerce/1077966-ecommerce/png/009-wishlist.png">
                <p>Dispondrás de una lista con tus gustos y estilos.</p>


            </div>

        </div>
        @endsection
        @section('footer')
        @include('admin.partial.footerBuyWithMe')
        @endsection
    </div>

    </div>
    @section('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <script src="js/dropdown.js"></script>
    @endsection
</body>

</html>