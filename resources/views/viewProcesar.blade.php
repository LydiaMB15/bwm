<html>
<head>
    @yield('style')
    <style>
      /* Set the size of the div element that contains the map */
      #map {
        height: 500px;  /* The height is 400 pixels */
        width: 100%;  /* The width is the width of the web page */
        background-image: url("imagenes/loading-animated-gif.gif");
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-position: center; 
        border: 1px solid #000000;
       }

</style>
</head>
<body>

<div class="container-fluid"> 
  
    @yield('nav')
    <div>
    @foreach ($marker as $mark)
      {{$mark->name}}
      {{$mark->address}}
      
    @endforeach
  

    </div>    
    @yield('subfooter')
    @yield('footer')
    @yield('scripts')
    <script  language="javascript" src="{{asset('js/mostrarMapa.js')}}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD9VkNJqr5d0zdYtrAd8j8iE2rjm_0doKc&libraries=places&callback=initMap" async defer></script>
    <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="  crossorigin="anonymous"></script>
  
    

</div>
</body>
</html>