@extends('layout')

@section('content')
    <section class="pages container">
        <div class="page page-about">
            <h1 class="text-capitalize">Página no encontrada</h1>
            <p>Volver a <a href="{{ route('blog.show') }}">Inicio</a></p>
        </div>
    </section>
@endsection
