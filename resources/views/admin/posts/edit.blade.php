@extends('admin.layout')
@section('links')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/dropzone.css">
<!-- Select2 -->
<link rel="stylesheet" href="/adminlte/plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="/adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

@endsection

@section('header')
<div class="col-sm-6">
    <h1 class="m-0 text-dark">Crear publicación</h1>
</div><!-- /.col -->
<div class="col-sm-6">
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Inicio</a></li>
        <li class="breadcrumb-item active"><a href="{{ route('admin.posts.index') }}">Posts</a></li>
    </ol>
</div><!-- /.col -->

@endsection

@section('content')

<form method="POST" action="{{ route('admin.posts.update', $post) }}">
    

    {{ csrf_field() }} 
    {{ method_field('PUT')}}
    <div class="row">
        <div class="col-md-8">
            <div class="card text-center">
                <div class="card-header">
                    <h3 class="card-title">Crear un nuevo post</h3>

                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label class="form-control @error('title') is-invalid @enderror">Título de la
                            publicación</label>
                        <input name="title" value="{{ old('title', $post->title) }}" class="form-control"
                            placeholder="introduce el título de la publicación">
                        {!! $errors->first('title','<span class="help-block text-danger">:message</span>') !!}
                    </div>

                    <div class="form-group">

                        <label class="form-control @error('body') is-invalid @enderror">Contenido de la
                            publicación</label>

                        <textarea class="textarea" name="body" placeholder="Introduce el contenido aquí"
                            style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ old('body' , $post->body) }}</textarea>

                        {!! $errors->first('body','<span class="help-block text-danger">:message</span>') !!}
                    </div>
                    <div class="form-group">

                        <label class="form-control @error('iframe') is-invalid @enderror">Videos o audios de la
                            publicación</label>

                        <textarea name="iframe" placeholder="Introduce el contenido de videos"
                            style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ old('iframe' , $post->iframe) }}</textarea>

                        {!! $errors->first('iframe','<span class="help-block text-danger">:message</span>') !!}
                    </div>

                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card text-center">
                <div class="card-header">
                    <h3 class="card-title">Extracto y categorías</h3>
                </div>

                <div class="card-body">
                    <div class="form-group">
                        <label class="form-control @error('excerpt') is-invalid @enderror">Extracto de la
                            publicación</label>
                        <textarea name="excerpt" class="form-control"
                            placehorlder="introduce el extracto de la publicación">{{ old('excerpt', $post->excerpt) }}</textarea>
                        {!! $errors->first('excerpt','<span class="help-block text-danger">:message</span>') !!}
                    </div>
                    <!-- Date dd/mm/yyyy -->
                    <div class="form-group">
                        <label class="form-control">Fecha de publicación:</label>

                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                            </div>
                            <input type="text" name="published_at"
                                value="{{ old('published_at', $post->published_at ? $post->published_at->format('m/d/Y') : null) }}"
                                class="form-control" data-inputmask-alias="datetime" placeholder="dd/mm/yyyy"
                                data-inputmask-inputformat="dd/mm/yyyy" data-mask>

                        </div>
                        <!-- /.input group -->
                    </div>
                    <div class="form-group">
                        <label class="form-control @error('category_id') is-invalid @enderror">Categorías</label>
                        <select name="category_id" class="form-control select2" style="width: 100%;">
                            <option value="">Selecciona una categoria</option>
                            @foreach ($categories as $category)
                            <option value="{{ $category->id }}"
                                {{ old('category_id', $post->category_id) == $category->id ? 'selected' : '' }}>
                                {{ $category->name }}</option>

                            @endforeach
                        </select>
                        {!! $errors->first('category_id','<span class="help-block text-danger">:message</span>') !!}
                    </div>

                     <div class="form-group">
                        <label class="form-control @error('tags') is-invalid @enderror">Selecciona etiquetas</label>
                        <select class="form-control select2" multiple="multiple" value="{{ old('tags[]') }}" name="tags[]"
                            data-placeholder="Selecciona una o mas etiquetas" style="width: 100%;">
                            @foreach($tags as $tag)
                            <option
                                {{ collect(old('tags', $post->tags->pluck('id')))->contains($tag->id) ? 'selected' : ''}}
                                value="{{ $tag->id }}">{{ $tag->name }}</option>
                            @endforeach

                        </select>
                        {!! $errors->first('tags','<span class="help-block text-danger">:message</span>') !!}
                    </div> 
                    <div class="form-group">
                        <div class="dropzone">

                        </div>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block">Guardar Publicación</button>
                    </div>
                </div>


            </div>

        </div>
    </div>
</form>
@if ($post->photos->count())
<div class="col-md-12">
    <div class="card text-center">
        <div class="card-header">
            <h3 class="card-title">Fotos de la publicación</h3>
        </div>

        <div class="card-body">
            <div class="row">

                @foreach($post->photos as $photo)
                <div class="col-md-3">
                    <form method="POST" action="{{ route('admin.photos.destroy',$photo) }}">

                        {{ method_field('DELETE') }} {{ csrf_field() }}


                        <button class="btn btn-danger btn-xs float-left"><i class="fa fa-times"></i></button>
                        <img class="img-thumbnail" src="{{ url($photo->url) }}">

                    </form>
                </div>
                @endforeach


            </div>
        </div>
    </div>
</div>
@endif

</div>

@endsection
@section('script')


<!-- Select2 -->
<script src="/adminlte/plugins/select2/js/select2.full.min.js"></script>
<script>
$(function() {
    //Initialize Select2 Elements
    $('.select2').select2({
        tags: true
    })
})
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js"></script>
<script>
Dropzone.autoDiscover = false;
$('.dropzone').dropzone({
    url: '/admin/posts/{{ $post->url }}/photos',
    headers: {
        'X-CSRF-TOKEN': '{{ csrf_token() }}'
    },
    maxFilesize: 25,
    paramName: 'photo',
    dictDefaultMessage: 'Arrastra las fotos aqui para subirlas',
    acceptedFiles: 'image/*',
});
</script>

@endsection