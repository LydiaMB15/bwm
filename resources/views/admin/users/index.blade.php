@extends('admin.layout')

@section('header')
<div class="col-sm-12">
    <h1 class="m-0 text-dark">Todo los usuarios</h1>

</div><!-- /.col -->
<div class="col-sm-12">
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Inicio</a></li>
        <li class="breadcrumb-item active"><a href="{{ route('admin.users.index') }}">Usuarios</a></li>
    </ol>
</div><!-- /.col -->
<div class="col-sm-12">
    <a href="{{ route('admin.users.create') }}" class="btn btn-primary float-right">
        <i class="fa fa-plus">Crear usuarios</i>
    </a>
</div>

@endsection

@section('content')

<div class="col-md-12">

    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Todos los usuarios</h3>
        </div>

        <div class="card-body">
            <table id="users-table" class="display responsive nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Email</th>
                        <th>Roles</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email}}</td>
                        <td>{{ $user->getRoleNames()->implode(',') }}</td>
                        <td>
                            <a href="{{ route('admin.users.show', $user )}}" class="btn btn-xs btn-light"><i
                                    class="fa fa-eye"></i></a>
                            <a href="{{ route('admin.users.edit', $user )}}" class="btn btn-xs btn-info"><i
                                    class="fa fa-magic"></i></a>
                            <form method="POST" action="{{ route('admin.users.destroy',$user) }}"
                                style="display: inline">
                                {{ csrf_field() }} {{ method_field('DELETE') }}
                                <button class="btn btn-xs btn-danger"
                                    onclick="return confirm('¿Estás seguro de eliminar este usuario?')"><i
                                        class="fa fa-times"></i></button>
                            </form>


                        </td>
                    </tr>
                    @endforeach
                </tbody>

            </table>

        </div>
        <!-- </div> -->
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
    aria-hidden="true">
    <form method="POST" action="{{ route('admin.users.store') }}">
        {{ csrf_field() }}

        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Introduce el título de la usuario</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="form-control @error('title') is-invalid @enderror">Título del
                            usuario</label>
                        <input name="title" value="{{ old('title') }}" class="form-control"
                            placeholder="introduce el título de la usuario">

                        {!! $errors->first('title','<span class="help-block text-danger">:message</span>') !!}

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button class="btn btn-primary">Crear usuario</button>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection