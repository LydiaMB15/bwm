@extends('admin.layout')

@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title">Datos Personales</h3>
            </div>
            <div class="card-body">
                @if ($errors->any())
                <ul class="list-group">
                    @foreach ($errors->all() as $error)
                    <li class="list-group-item list-group-item-danger">{{ $error }}</li>
                    @endforeach
                </ul>
                @endif

                <form method="POST" action=" {{ route('admin.users.update', $user) }}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <div class="form-group">
                        <label for="name">Nombre:</label>
                        <input name="name" value="{{ old('name',$user->name) }}" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="surname1"> Primer Apellido
                        </label>


                        <input id="surname1" type="text" class="form-control @error('surname1') is-invalid @enderror"
                            name="surname1" value="{{ old('surname1',$user->surname1) }}" required autocomplete="name" autofocus>

                        @error('surname1')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>


                    <div class="form-group">
                        <label for="surname2">Segundo Apellido</label>


                        <input id="surname2" type="text" class="form-control @error('surname2') is-invalid @enderror"
                            name="surname2" value="{{ old('surname2',$user->surname2) }}" required autocomplete="name" autofocus>

                        @error('surname2')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>



                    <div class="form-group">
                        <label for="cbx_comunidad">Seleccione
                            Comunidad:</label>                               
                        <select data-id="{{ $user->comunidad_id ?: '' }}" class="form-control @error('cbx_comunidad') is-invalid @enderror" name="cbx_comunidad"
                            id="cbx_comunidad" required>
                        </select>

                        @error('cbx_comunidad')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>


                    <div class="form-group">
                        <label for="cbx_provincia">Seleccione
                            Provincia:</label>


                        <select data-id="{{ $user->provincia_id ?: '' }}" class="form-control @error('cbx_provincia') is-invalid @enderror" name="cbx_provincia"
                            id="cbx_provincia" required>

                        </select>

                        @error('cbx_provincia')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="municipio">Seleccione
                            Municipio:</label>


                        <select data-id="{{ $user->municipio_id ?: '' }}" class="form-control @error('cbx_municipio') is-invalid @enderror" name="cbx_municipio"
                            id="cbx_municipio" required>
                            <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />

                            <input id="longitud" name="longitud" type="hidden" value="">
                            <input id="latitud" name="latitud" type="hidden" value="">

                        </select>

                        @error('cbx_municipio')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="email">Email:</label>
                        <input name="email" value="{{ old('email',$user->email) }}" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="password">Contraseña:</label>
                        <input type="password" name="password" placeholder="Contraseña" class="form-control">
                        <span class="help-block">Dejar en blanco para no cambiar la contraseña</span>
                    </div>
                    <div class="form-group">
                        <label for="password_confirmation">Repite la contraseña:</label>
                        <input type="password" name="password_confirmation" placeholder="Repite la contraseña"
                            class="form-control">

                    </div>
                    <div class="form-group">
                        <label>Roles</label>
                        @include('admin.roles.checkboxes')
                    </div>

                    <div class="form-group">
                        <label>Permisos</label>
                        @include('admin.permissions.checkboxes')
                    </div>

        
                    <button class="btn btn-primary btn-block">Actualizar usuario</button>
                </form>
            </div>

        </div>
    </div>
    <div class="col-md-6">
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title">Roles</h3>
            </div>
            <div class="card-body">
                <form method="POST" action="{{ route('admin.users.roles.update', $user) }}">
                    {{ csrf_field() }} {{ method_field('PUT') }}
                    @include('admin.roles.checkboxes')
                    <button class="btn btn-primary btn-block">Actualizar roles</button>
                </form>
            </div>
        </div>

        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title">Permisos</h3>
            </div>
            <div class="card-body">
                <form method="POST" action="{{ route('admin.users.permissions.update', $user) }}">
                    {{ csrf_field() }} {{ method_field('PUT') }}
                    @include('admin.permissions.checkboxes')
                    <button class="btn btn-primary btn-block">Actualizar permisos</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection