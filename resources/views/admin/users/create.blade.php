@extends('admin.layout')

@section('content')
<div class="row">
    <div class="card card-primary card-outline">
        <div class="card-header">
            <h3 class="card-title">Datos Personales</h3>
        </div>
        <div class="card-body">
            @if ($errors->any())
            <ul class="list-group">
                @foreach ($errors->all() as $error)
                <li class="list-group-item list-group-item-danger">{{ $error }}</li>
                @endforeach
            </ul>
            @endif

            <form method="POST" action=" {{ route('admin.users.store') }}">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="name">Nombre:</label>
                    <input name="name" value="{{ old('name') }}" class="form-control">
                </div>
                <div class="form-group">
                    <label for="surname1"> Primer Apellido
                    </label>


                    <input id="surname1" type="text" class="form-control @error('surname1') is-invalid @enderror"
                        name="surname1" value="{{ old('surname1') }}" required autocomplete="name" autofocus>

                    @error('surname1')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>


                <div class="form-group">
                    <label for="surname2">Segundo Apellido</label>


                    <input id="surname2" type="text" class="form-control @error('surname2') is-invalid @enderror"
                        name="surname2" value="{{ old('surname2') }}" required autocomplete="name" autofocus>

                    @error('surname2')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>



                <div class="form-group">
                    <label for="cbx_comunidad">Seleccione
                        Comunidad:</label>

                    <select class="form-control @error('cbx_comunidad') is-invalid @enderror" name="cbx_comunidad"
                        id="cbx_comunidad" required>
             
                    </select>

                    @error('cbx_comunidad')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>


                <div class="form-group">
                    <label for="cbx_provincia">Seleccione
                        Provincia:</label>


                    <select class="form-control @error('cbx_provincia') is-invalid @enderror" name="cbx_provincia"
                        id="cbx_provincia" required>

                    </select>

                    @error('cbx_provincia')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
      
        <div class="form-group">
            <label for="municipio">Seleccione
                Municipio:</label>


            <select class="form-control @error('cbx_municipio') is-invalid @enderror" name="cbx_municipio"
                id="cbx_municipio" required>
                <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />

                <input id="longitud" name="longitud" type="hidden" value="">
                <input id="latitud" name="latitud" type="hidden" value="">

            </select>

            @error('cbx_municipio')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="form-group">
            <label for="email">Email:</label>
            <input name="email" value="{{ old('email') }}" class="form-control">
        </div>

        <div class="form-group">
            <label>Roles</label>
            @include('admin.roles.checkboxes')
        </div>

        <div class="form-group">
            <label>Permisos</label>
            @include('admin.permissions.checkboxes')
        </div>
        <span class="help-block">La contraseña será generada y enviada al usuario vía email</span><br><br>
        <button class="btn btn-primary btn-block">Crear usuario</button>
        </form>
    </div>

</div>
</div>
@endsection

