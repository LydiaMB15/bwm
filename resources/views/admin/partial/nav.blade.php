      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
           <li class="nav-item">            
            <a href="{{ route('dashboard') }}" class="nav-link">Inicio
              <i class="nav-icon fas fa-home"></i>
              <p>

              </p>
            </a>
          </li>
          <li class="nav-item">            
            <a href="{{ route('admin.posts.index') }}" class="nav-link">Ver Posts
              <i class="nav-icon fas fa-eye"></i>
              <p>

              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" data-toggle="modal" data-target="#exampleModalLong" class="nav-link">Crear Post
              <i class="nav-icon fas fa-th"></i>
              <p>

              </p>
            </a>
          </li>
          <li class="nav-item">            
            <a href="{{ route('admin.users.index') }}" class="nav-link">Ver Usarios
              <i class="nav-icon fas fa-eye"></i>
              <p>

              </p>
            </a>
          </li>
          <li class="nav-item">            
            <a href="{{ route('admin.users.create') }}" class="nav-link">Crear usuario
              <i class="nav-icon fas fa-th"></i>
              <p>

              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('admin.escrito.index') }}" class="nav-link">Crear Escrito
              <i class="nav-icon fas fa-th"></i>
              <p>

              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->