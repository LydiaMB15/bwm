<?php
use App\Post;
use Carbon\Carbon;
use App\Category;
use Illuminate\Database\Seeder;
use App\Tag;
use Illuminate\Support\Facades\Storage;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Storage::disk('public')->deleteDirectory('posts');
        Post::truncate();
        Category::truncate();
        Tag::truncate();

        $category = new Category;
        $category->name = "Categoria1";
        $category->save();

        $category = new Category;
        $category->name = "Categoria2";
        $category->save();
        

        $post = new Post;
        $post->title = "Mi primer post";
        $post->url = Str::slug("Mi primer post");
        $post->excerpt = "Mi primer post";
        $post->body="<p>Contenido de mi primer post</p>";
        $post->published_at = Carbon::now();
        $post->category_id = 1;
        $post->user_id = 1;
        $post->save();
        $post->tags()->attach(Tag::create(['name' => 'etiqueta1']));


        $post = new Post;
        $post->title = "Mi segundo post";
        $post->url = Str::slug("Mi segundo post");
        $post->excerpt = "Mi segundo post";
        $post->body="<p>Contenido de mi segundo post</p>";
        $post->published_at = Carbon::now();
        $post->category_id = 2;
        $post->user_id = 1;
        $post->save();
        $post->tags()->attach(Tag::create(['name' => 'etiqueta2']));

        $post = new Post;
        $post->title = "Mi tercer post";
        $post->url = Str::slug("Mi tercer post");
        $post->excerpt = "Mi tercer post";
        $post->body="<p>Contenido de mi tercer post</p>";
        $post->published_at = Carbon::now();
        $post->category_id = 2;
        $post->user_id = 2;
        $post->save();
        $post->tags()->attach(Tag::create(['name' => 'etiqueta2']));

        $post = new Post;
        $post->title = "Mi cuarto post";
        $post->url = Str::slug("Mi cuarto post");
        $post->excerpt = "Mi cuarto post";
        $post->body="<p>Contenido de mi cuarto post</p>";
        $post->published_at = Carbon::now();
        $post->category_id = 2;
        $post->user_id = 2;
        $post->save();
        $post->tags()->attach(Tag::create(['name' => 'etiqueta2']));

    }
}