<?php

use Illuminate\Database\Seeder;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::truncate();
        Role::truncate();
        User::truncate();

        $viewPostPermission = Permission::create(['name' => 'View posts']);
        $create = Permission::create(['name' => 'Create posts']);
        $updatePostPermission = Permission::create(['name' => 'Update posts']);
        $deletePostPermission = Permission::create(['name' => 'Delete posts']);

        $adminRole = Role::create(['name' => 'Admin']);
        $writerRole = Role::create(['name' => 'Writer']);

        $admin = new User();
        $admin->name = 'Lydia';
        $admin->surname1 = 'Manzanares';
        $admin->surname2 = 'Borreguero';
        $admin->comunidad_id = 10;
        $admin->provincia_id = 3;
        $admin->municipio_id = 168;
        $admin->email = 'lydiamb12@gmail.com';
        $admin->password = bcrypt('123123');

        $admin->save();
        $admin->assignRole($adminRole);


        $witer = new User();
        $witer->name = 'Lydia';
        $witer->surname1 = 'Manzanares';
        $witer->surname2 = 'Borreguero';
        $witer->comunidad_id = 10;
        $witer->provincia_id = 3;
        $witer->municipio_id = 168;
        $witer->email = 'lydiamb_@hotmail.com';
        $witer->password = bcrypt('123123');

        $witer->save();
        $witer->assignRole($writerRole);

    }
}
