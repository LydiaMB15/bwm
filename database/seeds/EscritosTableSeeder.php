<?php

use Illuminate\Database\Seeder;
use App\Escrito;
use App\Parte;

class EscritosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Escrito::truncate();
        Parte::truncate();

        $parte = new Parte();
        $parte->id = '1';
        $parte->name = 'triptico superior';
        $parte->save();

        $parte = new Parte();
        $parte->id = '2';
        $parte->name = 'triptico inferior';
        $parte->save();

        $escrito = new Escrito();
        $escrito->title = 'Soy Cliente';
        $escrito->body = 'Me gusta tener a alguien que me guíe a la hora de comprar, pero no siempre mis conocidos pueden venir conmigo, me divierto cuando mi asesor me acompaña, saco provecho de las horas de compra, siempre me llevo algo que me encanta! me pruebo los modelos que mejor me quedan sin salir del probador! así da gusto comprar!';
        $escrito->button_title = 'Quiero ser Cliente';
        $escrito->parte_id = '1';
        $escrito->save();
        

        $escrito = new Escrito();
        $escrito->title = 'Soy Asesor';
        $escrito->body = 'Me divierto comprando, me divierto dándo a la gente lo mejor de mí, los consejos de moda que más me gustan, me gusta ser amable y que queden satisfechos con la compra que hacen. Me encanta cuando la gente está feliz gracias a mi ayuda y colaboración. A menudo me tomo un café con mis clientes y entablo amistades! Es la mejor forma de invertir mi tiempo!';
        $escrito->button_title = 'Quiero ser Asesor';
        $escrito->parte_id = '1';
        $escrito->save();


        $escrito = new Escrito();
        $escrito->title = 'Soy Colaborador';
        $escrito->body = 'Mi comercio está disponible en esta web, porque me permite ofrecer a mis clientes un servicio de valor añadido muy apreciado por los consumidores. Ellos acuden a mi comercio, acompañados por los mejores asesores, que suerte que desde entonces mi comercio se llena! Esta web me ofrece la visibilidad que antes no tenía!';
        $escrito->button_title = 'Quiero ser Colaborador';
        $escrito->parte_id = '1';
        $escrito->save();


    }
}
