<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UltimosMarkersBuscados extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ultimos_markers_buscados', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',60);
            $table->string('address',80);
            $table->float('lat',10, 6);
            $table->float('lng',10,6);
            $table->string('type',30);
            $table->string('idG',50);
            $table->integer('idMarker');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('ultimos_markers_buscados');
    }
}
