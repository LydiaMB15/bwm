<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
class PostsController extends Controller
{
    public function show(Post $post)
    {
        // var_dump($post->isPublished());
        if($post->isPublished() || auth()->check())
        {
            return view('posts.show',compact('post'));
        }
        abort(404);
    }
}
