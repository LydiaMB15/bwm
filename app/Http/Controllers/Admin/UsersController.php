<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Http\Requests\UpdateUserRequest;
use Illuminate\Support\Str;
use App\Comunidad;
use Illuminate\Validation\Rule;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();

        return view('admin.users.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = new User;
        $comunidades = Comunidad::all();
        $roles = Role::with('permissions')->get();
        $permissions = Permission::pluck('name','id');

        return view('admin.users.create',compact('user','roles','permissions','comunidades'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pos = strpos($request['cbx_municipio'], '{');
        $municipio = substr($request['cbx_municipio'], 0, $pos);
            
        $data = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'surname1' => ['required', 'string'],
            'surname2' => ['required', 'string'],
            'cbx_comunidad' => ['required'],
            'cbx_provincia' => ['required'],
            'cbx_municipio' => ['required'],
        ]);

        
        $data['password'] = Str::random(8);

        $user = User::create([
            'name' => $data['name'],
            'email' =>$data['email'],
            'password' => $data['password'],
            'surname1' => $data['surname1'],
            'surname2' => $data['surname2'],                        
            'comunidad_id' => $data['cbx_comunidad'],
            'provincia_id' => $data['cbx_provincia'],  
            'municipio_id' => $municipio,
            
        ]);

        $user->assignRole($request->roles);

        $user->givePermissionTo($request->permissions);

        return redirect()->route('admin.users.index')->withFlash('El usuario ha sido creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('admin.users.show',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //$roles = Role::pluck('name','id');

        $comunidades = Comunidad::all();
        $roles = Role::with('permissions')->get();
        $permissions = Permission::pluck('name','id');

        return view('admin.users.edit',compact('user','roles','permissions','comunidades'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        $user->update($request->validated());
        
        return back()->withFlash('Usuario actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
