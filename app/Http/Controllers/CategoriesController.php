<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoriesController extends Controller
{
    public function show(Category $category)
    {
        $posts = $category->posts;

        return view('welcome', [
            'title' => "Publicaciones de la catoría $category->name ",
            'posts' => $category->posts()->paginate(15)
        ]);
    }
}
