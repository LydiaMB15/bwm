<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comunidad;
use APP\Provincia;
use App\Escrito;
use App\Parte;

class ComunidadController extends Controller
{
    public function show()
    {
        return 'hola';
    }
    
    public function getComunidades(Request $request){
        if($request-> ajax() ){
           $comunidades = Comunidad::comunidades();
            return response()->json($comunidades);

        }
  
    }

    public function index(){
        //$comunidades = Comunidad::all();
        $escritos = Escrito::all();
        $partes = Parte::all();
        return view('index',compact('escritos','partes'));
     
    }
}
