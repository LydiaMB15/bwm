<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Users_busquedas;
use App\Comunidad;
use App\Provincia;
use App\Municipio;
use App\Marker;
use Symfony\Component\VarDumper\VarDumper;

class MapaController extends Controller
{
    public function saludo()
    {
        return 'Hola mundo';

    }

    public function viewLocations(Request $request)
    {
        //dd($request->all());  //to check all the datas dumped from the form
        
        $this->validate(request(), [
            'cbx_comunidad' => 'required',
            'cbx_provincia' => 'required',
            'cbx_municipio' => 'required'
        ]);
     

        $busqueda = new Users_busquedas();
        
        $busqueda->cbx_comunidad = $request->cbx_comunidad;
        $busqueda->cbx_provincia = $request->cbx_provincia;
        
        //$busqueda->cbx_municipio = $request->cbx_municipio;
      
        $pos = strpos($request->cbx_municipio, '{');
        $busqueda->cbx_municipio = substr($request->cbx_municipio,0,$pos);
       
        $busqueda->latitud = $request->latitud;
        $busqueda->longitud = $request->longitud;

        $time = time();
        $busqueda->fecha = date("Y-m-d", $time);
        $busqueda->hora = date("H:i:s", $time);
        $busqueda->save();

        $comunidades = Comunidad::all();
        
        //View locations finded
        $markers = Marker::marcadoresSesion();
        
        $comun = Comunidad::getComunidad($busqueda->cbx_comunidad);
        $provincia = Provincia::getProvincia($busqueda->cbx_provincia);
        $municipio = Municipio::getMunicipio($busqueda->cbx_municipio);
        //dd($markers);{}
        
        return view('mapa', compact('busqueda','comunidades','markers','comun','provincia','municipio'));

    }

    public function recogerForm(Request $request)
    {
        
        $marker = new Marker;
        $marker->name = $request->name;
       
        $marker->address = $request->address;

        $marker->idG = $request->idG;

       
        $pos = strpos($request->latlong, ',');
        $latitud = substr($request->latlong, 1, $pos - 1);
        $longitud = substr($request->latlong, $pos + 1, strlen($request->latlong));
       
        $marker->lat = $latitud;
        $marker->lng = $longitud;
        $marker->idMarker = $request->id;
        $marker->type = $request->types;
        
        $marker->session = session()->getId();

        if ($marker::existeMarcadorSession($marker->idG) == null)
        {
            $marker->save();

        }
        
        return $request;

    }






}
