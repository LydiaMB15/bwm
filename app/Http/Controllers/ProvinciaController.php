<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comunidad;
use App\Provincia;

class ProvinciaController extends Controller
{
    public function getProvincias(Request $request, $id){
        if($request-> ajax() ){
           $provincias = Provincia::provincias($id);
            return response()->json($provincias);
            // return $provincias;
          }
  
      }
 
    
}
