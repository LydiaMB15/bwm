<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => ['required', 'string', 'max:255'],
            'email' => [
                'required',
                Rule::unique('users')->ignore($this->route('user')->id)
            ],
            'surname1' => ['required', 'string'],
            'surname2' => ['required', 'string'],
            'cbx_comunidad' => ['required'],
            'cbx_provincia' => ['required'],
            'cbx_municipio' => ['required'],
        ];

        if ($this->filled('password'))
        {
            $rules['password'] = ['confirmed','min:6'];
        }

        return $rules;
        
    }
}
