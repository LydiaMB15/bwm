<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comunidad extends Model
{
    protected $table = 'comunidades';


    //todas las comunidades
    public static function comunidades()
    {
        return Comunidad::all();
    }

    public static function getComunidad($id)
    {
        return Comunidad::where('id', '=', $id)
                        ->get();
    }

}