<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Municipio extends Model
{
    protected $table = 'municipios';

    //todas los municipios pertenecientes a la provincia
    public static function municipios($id)
    {
        return Municipio::where('provincia_id', '=', $id)
            ->get();
    }

    public static function getMunicipioAsesor($cbx_municipio)
    {
        return Municipio::where('id','=',$cbx_municipio)
        ->get();
    }

    public static function getMunicipio($id)
    {
        return Municipio::where('id', '=', $id)
                        ->get();
    }
}
