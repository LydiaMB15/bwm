<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Provincia extends Model
{
    protected $table = 'provincias';

    //todas las provincias pertenecientes a un comunidad
    public static function provincias($id)
    {
        return Provincia::where('comunidad_id', '=', $id)
            ->get();
    }

    public static function getProvinciaAsesor($cbx_provincia)
    {
        return Provincia::where('id', '=', $cbx_provincia)
            ->get();
    }

    public static function getProvincia($id)
    {
        return Provincia::where('id', '=', $id)
                        ->get();
    }

}
