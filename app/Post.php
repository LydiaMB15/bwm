<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Post extends Model
{

    protected $fillable = [     

        'title','body','iframe','excerpt','published_at','category_id','user_id',     
    ];

   // protected $guarded = [];
    protected $dates = ['published_at'];

    public function category() //$post->category->namespace
    {
        return $this->belongsTo(Category::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);

    }
    
    public function scopePublished($query) //querybuilder//queryscope
    {
        $posts =  Post::latest('published_at')
                    ->where('published_at','<=',Carbon::now());
                  
    }

    public function scopeAllowed($query)
    {
        if(auth()->user()->can('view', $this))
        {
            return $query;
        }
     
        return $query->where('user_id', auth()->id());
      
    }
    

    public function isPublished()
    {
        return !is_null($this->published_at) || $this->published_at <= today();
            
    }


     /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'url';
    }

 
    public function photos()
    {
        return $this->hasMany(Photo::class);
    }
  
    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    /*
    public function photos()
    {
        return $this->morphMany(Photo::class,'photoable');
    }
    */
    public function setPublishedAtAttribute($published_at)
    {
        $this->attributes['published_at'] = Carbon::parse($published_at);;
    }

    public function setCategoryIdAttribute($category)
    {
        $this->attributes['category_id'] = Category::find($category) 
                                                    ? $category
                                                    : Category::create(['name' => $category])->id;
    }

    public function syncTags($tags)
    {
        $tagIds = collect($tags)->map(function($tag){
            return Tag::find($tag)
                        ? $tag  
                        : Tag::create(['name' => $tag])->id;
        });

        return $this->tags()->sync($tagIds);

    }

    public function viewType($home = '')
    {
        if ($this->photos->count() == 1):
            return 'posts.photo';
        elseif($this->photos->count() > 1):
            return $home === 'home' ? 'posts.carousel-preview' : 'partial.carousel';
        elseif($this->iframe):
            return 'posts.iframe';
        else:
            return 'posts.text';
        endif;
    }

    protected static function boot()
    {
        parent::boot();

        static::deleting(function($post){
            $post->tags()->detach();
        
            foreach($post->photos as $photo)
            {
                $photo->delete();
            }
        });
    }
    


}